# nova-rollup-template

A template for building [Nova extensions][api] that require bundling using [Rollup.js][rollup].

[api]: https://docs.nova.app/
[rollup]: https://rollupjs.org/
