import commonjs from "@rollup/plugin-commonjs";
import {nodeResolve} from "@rollup/plugin-node-resolve";

module.exports =
{
	input: "src/scripts/main.js",
	output: {
		file: "Extension.novaextension/scripts/main.dist.js",
		format: "cjs",
		exports: "named",
	},
	plugins: [
		commonjs(),
		nodeResolve(),
	],
};
